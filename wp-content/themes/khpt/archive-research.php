<?php
   /**
    * The main template file
    *
    * This is the most generic template file in a WordPress theme
    * and one of the two required files for a theme (the other being style.css).
    * It is used to display a page when nothing more specific matches a query.
    * E.g., it puts together the home page when no home.php file exists.
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package WordPress
    * @subpackage Ihat
    * @since 1.0.0
    */
   get_header();
   $terms = get_terms(array(
       'taxonomy' => 'Researh',
       'hide_empty' => false,
   ));
   
   $area = get_field_object("area")['choices'];
   $category = get_field_object("category")['choices'];
   $type = get_field_object("type")['choices'];
   
   
   $selectedYears = '';
   if (isset($_GET['selectedYears'])) {
       $selectedYears = $_GET['selectedYears'];
   }
   
   $selectedAreas = '';
   if (isset($_GET['selectedAreas'])) {
       $selectedAreas = $_GET['selectedAreas'];
   }
   
   $selectedCategory = '';
   if (isset($_GET['selectedCategory'])) {
       $selectedCategory = $_GET['selectedCategory'];
   }
   
   $selectedType = '';
   if (isset($_GET['selectedType'])) {
       $selectedType = $_GET['selectedType'];
   }
   global $post;
   ?>
<section id="contact-us" class="banner">
<div class="container-fluid container-sec-one">
   <div class="row no-gutters banner-wrap" style="
      background-image: url(<?php the_field('research_banner', 'option'); ?>);background-repeat: no-repeat;
      background-size: cover;
      background-position: center;">
      <div class="col-12 text-center">
         <div class="health-care">
            <h1 data-aos="fade-up"><?php the_field('heading', 'option'); ?></h1>
            <hr style="margin-left: 45%;">
         </div>
      </div>
   </div>
</div>
<section id="research-option" class="research-option">
   <div class="container my-lg-5">
      <form id="research-filter" method="get">
         <div class="row text-center">
            <div class="col-6 col-md-2 right-border" data-aos="fade-up">
               <select name="selectedYears">
                  <option value="">Years</option>
                  <?php foreach ($terms as $item) {
                     ?>
                  <option value="<?php echo $item->slug; ?>" <?php echo ($selectedYears == $item->slug) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                  <?php
                     } ?>
               </select>
            </div>
            <div class="col-6 col-md-2 right-border" data-aos="fade-up">
               <select name="selectedAreas" >
                  <option value="">Areas</option>
                  <?php foreach ($area as $areas) {
                     ?>
                  <option value="<?php echo $areas; ?>" <?php echo ($selectedAreas == $areas) ? 'selected' : ''; ?>><?php echo $areas; ?></option>
                  <?php
                     } ?>
               </select>
            </div>
            <div class="col-6 col-md-2 right-border" data-aos="fade-up">
               <select name="selectedCategory">
                  <option value="">Category</option>
                  <?php foreach ($category as $categorys) {
                     ?>
                  <option value="<?php echo $categorys; ?>" <?php echo ($selectedCategory == $categorys) ? 'selected' : ''; ?>><?php echo $categorys; ?></option>
                  <?php
                     } ?>
               </select>
            </div>
            <div class="col-6 col-md-2" data-aos="fade-up">
               <select name="selectedType">
                  <option value="">Type</option>
                  <?php foreach ($type as $types) {
                     ?>
                  <option value="<?php echo $types; ?>" <?php echo ($selectedType == $types) ? 'selected' : ''; ?>><?php echo $types; ?></option>
                  <?php
                     } ?>
               </select>
            </div>
            <div class="col-6 col-md-2"><a class="btn green-btn" role="button" href="#" id="btn-submit" target="_parent">Search</a></div>
            <div class="col-6 col-md-2"><a class="btn clear-btn" role="button" href="#"   id="btn-reset" target="_parent">Clear</a></div>
         </div>
      </form>
   </div>
</section>
<script>
   jQuery("#btn-submit").click(function() {
       jQuery("#research-filter").submit();
   })
   jQuery("#btn-reset").click(function() {
       $('#research-filter option:selected').removeAttr('selected');
   })
</script>
<section id="updates-news">
   <div class="container">
      <div class="row">
         <?php
            if (!empty($selectedYears)) {
                $tax_query = array(
                    array(
                        'taxonomy' => 'Researh',
                        'field' => 'slug',
                        'terms' => $selectedYears,
                        'compare' => 'LIKE',
                    ),
                );
            }
            
            $meta_query = array();
            if (!empty($selectedYears) && !empty($selectedAreas) && !empty($selectedCategory) && !empty($selectedType)) {
            
                $meta_query = array(
                    'relation' => 'AND',
                    array(
                        'key' => 'area',
                        'value' => $selectedAreas,
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key' => 'category',
                        'value' => $selectedCategory,
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key' => 'type',
                        'value' => $selectedType,
                        'compare' => 'LIKE'
                    )
                );
            } else if (!empty($selectedYears) && !empty($selectedAreas) && !empty($selectedCategory)) {
                $meta_query = array(
                    'relation' => 'AND',
                    array(
                        'key' => 'area',
                        'value' => $selectedAreas,
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key' => 'category',
                        'value' => $selectedCategory,
                        'compare' => 'LIKE'
                    )
                );
            } else if (!empty($selectedYears) && !empty($selectedAreas) && !empty($selectedType)) {
                $meta_query = array(
                    'relation' => 'AND',
                    array(
                        'key' => 'area',
                        'value' => $selectedAreas,
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key' => 'type',
                        'value' => $selectedCategory,
                        'compare' => 'LIKE'
                    )
                );
            } else if (!empty($selectedYears) && !empty($selectedAreas)) {
                $meta_query = array(
                    array(
                        'key' => 'area',
                        'value' => $selectedAreas,
                        'compare' => 'LIKE'
                    )
                );
            } else if (!empty($selectedYears) && !empty($selectedType)) {
            
                $meta_query = array(
                    array(
                        'key' => 'type',
                        'value' => $selectedAreas,
                        'compare' => 'LIKE'
                    )
                );
            } else if (!empty($selectedYears) && !empty($selectedCategory)) {
            
                $meta_query = array(
                    array(
                        'key' => 'category',
                        'value' => $selectedCategory,
                        'compare' => 'LIKE'
                    )
                );
            }
            $args = array(
                'post_type' => 'research',
                'post_status' => 'publish',
                'tax_query' => $tax_query,
                'meta_query' =>  $meta_query
            );
            
            $the_query = new WP_Query($args);
            // echo "<pre>";
            // print_r($the_query);
            // echo "</pre>";
            if ($the_query->have_posts()) :
                $i = 1;
                while ($the_query->have_posts()) :
                    $the_query->the_post();
            ?>
         <div class="col-12 col-md-4 mt-3" <?php echo $incre == 1 ? 'data-aos="fade-up"' : 'data-aos="fade-up"';?>>
            <div class="card">
               <div class="card-body">
                  <h5 class="text-orange"><?php echo get_the_title(); ?></h5>
                  <h6><?php echo get_the_date(); ?></h6>
                  <?php echo get_the_content(); ?>
               </div>
               <div class="card-text">
                  <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                  <div class="d-flex">
                     <?php if (get_field('link_pdf_download')) : ?>
                     <a class="text-orange download-link" href="<?php the_field('link_pdf_download'); ?>" target="_blank"><?php the_field('label_pdf_download'); ?>
                     </a>
                     <?php else: ?>
                     <a href="#" data-toggle="modal" data-target="#readmore-modal<?php echo $i; ?>">
                        <h6 class="text-orange"><?php the_field('label_text'); ?>&nbsp; &gt;&gt;</h6>
                     </a>
                     <?php endif; ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal fade" role="dialog" tabindex="-1" id="readmore-modal<?php echo $i; ?>">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
               <div class="modal-content">
                  <div class="modal-header">
                     <h1 class="modal-title"><?php echo get_the_title(); ?></h1>
                     <button type="button" class="close btn-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body">
                     <?php echo get_the_content(); ?>
                  </div>
                  <div class="modal-footer">
                     <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
         <?php
            $i++;
            endwhile;
            /* Restore original Post Data */
            wp_reset_postdata();
            ?>
         <?php else : ?>
         <?php _e('Something went wrong'); ?>
         <?php endif; ?>
         <div class="col-12 mb-3">
            <hr>
         </div>
      </div>
   </div>
</section>
<?php
   get_footer();
   ?>