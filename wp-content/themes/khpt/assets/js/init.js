(function ($) {
  $(document).ready(function () {
    //         Window Resize Ipad

    // navbar-menu-start
    $('.nav-button').click(function (e) {
      e.preventDefault();
      $('body').toggleClass('nav-open');
    });


    $(window).scroll(function () {
      if ($(this).scrollTop() > 40) {
        $("#nav-icon3 span").css("backgroundColor", "#063149");
        $('.navbar').addClass('navbar-bg');
        $('.banner-logo').hide();
      } else {
        if (window.location.pathname == "/") {
          $("#nav-icon3 span").css("backgroundColor", "white");
        }
        else {
          $("#nav-icon3 span").css("backgroundColor", "#063149");
        }
        $('.navbar').removeClass('navbar-bg');
        $('.banner-logo').show();
      }
    });



    if (window.location.pathname == "/") {
      $('.navbar-brand').addClass('navbar-img');
      $('.navbar-img').show();
    }
    else {
      $('.banner-logo').hide();
      $("#nav-icon3 span").css("backgroundColor", "#063149");
    }

    // Community Speaker

    var swiper = new Swiper('.community-wrapper', {
      spaceBetween: 30,
      centeredSlides: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });


    // Our Uniqua Dna start
    $('.click-dnaa').on('click', function (e) {
      e.preventDefault();
      // console.log(e);
      var $this = $(this);

      if ($this.hasClass('active')) {
        return;
      }
      $(".click-dna").removeClass('col-sm-4 col-sm-1 col-sm-10 active');
      $(".click-dna").each(function (index) {
        if ($this.attr("id") === $(this).attr("id")) {

        }
        $(this).addClass('col-sm-1');
        $('.closeFeature').css('left', '79%');
      });

      $this.addClass('active col-sm-10');
      if (!$("#dna-wrapper").hasClass('activate')) {
        $("#dna-wrapper").addClass('activate');
      }
      $(".closeFeature").show();

      if ($(this).attr("id") == 0) {
        $('.closeFeature').css('left', '79%');
      } else if ($(this).attr("id") == 1) {
        $('.closeFeature').css('left', '87%');
      } else {
        $('.closeFeature').css('left', '1060px');
      }
    })

    // Our Milestone  
    $('.closeFeature').on('click', function (e) {
      console.log(e);
      $("#dna-wrapper").removeClass('activate');
      $("#dna-wrapper .hover-state").removeClass('col-sm-1 active col-sm-10');
      $("#dna-wrapper .hover-state").addClass('col-sm-4');
      $(".closeFeature").hide();


    });



    // Our Milestone  start
    $('.journey-image').slick({
      infinite: true,
      asNavFor: '.journey-content, .journey-years, .journey-scroll',
      arrows: false,
      dots: true,
      // centerMode: true,
    });

    $('.journey-content').slick({
      infinite: true,
      prevArrow: $('.prev-btn'),
      nextArrow: $('.next-btn'),
      asNavFor: '.journey-image, .journey-years, .journey-scroll'
    });

    $('.journey-years').slick({
      infinite: true,
      slidesToShow: 8,
      slidesToScroll: 1,
      asNavFor: '.journey-content, .journey-image',
      focusOnSelect: true,
      arrows: false,
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1

        }
      }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });


    /**
     * Our Milestone
    */


    $(".mobile-card-header").click(function (e) {
      e.preventDefault();
      $(".mobile-card-header").removeClass("current");
      $(this).addClass('current');
      $(this).parent().parent().parent().parent().addClass('accordian-show-bg');


    });

    $(".close-icon").click(function () {
      $(".mobile-card-header").removeClass("current");
      $(this).parent().parent().parent().parent().removeClass('accordian-show-bg');

    });

    /**
     * Intents Meets Impacts counter
     */

    $(window).scroll(function () {
      if ($(this).scrollTop() > 2700) {
        $('.count').each(function () {
          $(this).prop('Counter', 0).animate({
            Counter: $(this).data("count")
          }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
              var number = Math.ceil(now);
              var formate = number.toLocaleString('en', {
                minimumFractionDigits: 0
              });
              $(this).text(formate);
            }
          });
        });
        console.log('test');
      }


    });

    //     voice-of ground-desktop-start
    var swiper = new Swiper('.voice_of_ground', {
      spaceBetween: 30,
      centeredSlides: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
    //    end

    //     priotries-start-desktop
    $('.priorities-carousel').slick({
      arrows: false,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 4,
      dots: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //     end


    //     priotries-start-desktop
    $('.banner-slider').slick({
      arrows: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //    		end

    //     our-project-start-desktop
    $('.our-projects').slick({
      prevArrow: '<i class="fa fa-angle-left slick-arrow-left"></i>',
      nextArrow: '<i class="fa fa-angle-right slick-arrow-right"></i>',
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //     end

    // previous-project-desktop-start
    $('.previous-project-row').slick({
      prevArrow: '<i class="fa fa-angle-left slick-arrow-left"></i>',
      nextArrow: '<i class="fa fa-angle-right slick-arrow-right"></i>',
      arrows: true,
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 3,
      dots: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: true,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //     end

    //     our-partners-desktop-start
    $('.our-partners').slick({
      prevArrow: '<i class="fa fa-angle-left slick-arrow-left"></i>',
      nextArrow: '<i class="fa fa-angle-right slick-arrow-right"></i>',
      arrows: true,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      autoplay: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            autoplay: true,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });


    /**
     Research
    */

    $(window).scroll(function () {
      if ($(this).scrollTop() > 220) {
        $('.research-option').addClass('fixed');
      } else {
        $('.research-option').removeClass('fixed');
      }
    });

    /**
    Our Focus scroll on position
   */

    $(window).on('load', function () {
      // Remove the # from the hash, as different browsers may or may not include it
      var hash = location.hash.replace('#', '');

      if (hash != '') {

        // Clear the hash in the URL
        // location.hash = '';   // delete front "//" if you want to change the address bar
        $('html, body').animate({
          scrollTop: $("#Our-Focus").offset().top - 100
        }, 2000);

      }

    });

    //         hr-border
    $(".border-hr:last").hide();
    //         end       end

    /**
     * animation script
     */
    AOS.init();

    /**
     * Navigation bar
     */
    if ($(window).width() <= 767) {       // if width is less than 600px
      $('.toggle-btn').click(function (e) {
        e.preventDefault();
        $(this).toggleClass("change");
        if ($(this).hasClass('change')) {
          $('.sidebar').css('right', '0%');
        }
        else {
          $('.sidebar').css('right', '-100%');
        }

      });            // execute mobile function
    }
    else {                              // if width is more than 600px
      $('.toggle-btn').click(function (e) {
        e.preventDefault();
        $(this).toggleClass("change");
        $('.sidemenu-toggle .collapse li').removeClass('pt-2 pb-2 side-menu-dropdown');
        $('.sidemenu-toggle .collapse li').addClass('pt-2');
        if ($(this).hasClass('change')) {
          $('.sidebar').css('right', '0');
          $('.navdemo').css('right', '322px');
          $('.navdemo').css('background-color', 'rgb(6, 49, 73)');
        }
        else {
          $('.sidebar').css('right', '-450px');
          $('.navdemo').css('right', '0px');
          $('.navdemo').css('background', 'none');
        }

      });               // execute desktop function
    }


    $('.sidemenu-toggle').click(function (e) {
      e.preventDefault();
      $('.sidemenu-toggle .collapse li').removeClass('pt-2 pb-2 side-menu-dropdown');
      $('.sidemenu-toggle .collapse li').addClass('pt-2');
    });

    // execute mobile function

    /**
     * Our Team
     */

    $('.team-content').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
    });


    $('.show_more').on('click', function () {
      console.log('satya');
      // var ID = $(this).attr('id');
      // $('.show_more').hide();
      // $('.loding').show();
      // $.ajax({
      //   type: 'POST',
      //   dataType: "json",
      //   url: my_ajax_object.ajax_url,
      //   data: {
      //     action: "get_news",
      //     data: {
      //       "channel": channel
      //     }
      //   },
      //   success: function (html) {
      //     $('#show_more_main' + ID).remove();
      //     $('.postList').append(html);
      //   }
      // });
    });


    $('.show_more').on('click', function (e) {
      console.log(e);
    });

  });
})(jQuery);