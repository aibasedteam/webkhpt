<?php

/**
 * Template Name:Focus Area
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Ihat
 * @since 1.0.0
 */
get_header();
if (have_posts()) :
    while (have_posts()) : the_post();
?>
        <div class="col">
            <div class="modal fade" role="dialog" tabindex="-1" id="myModal">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <h1 class="modal-title">Comprehensive Primary Health Care</h1><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <p>A strong primary healthcare system is crucial in order to achieve health related sustainable development goals (SDG) and attaining universal health coverage (UHC). In India, a series of transformative initiatives and increased
                                investments on primary health care had seen it achieve improvements in health promotion, disease prevention, and service outreach. However, services at primary care still remains sporadic and fragmented; high out-of-pocket
                                health expenditures remained static. A larger proportion of people are still accessing private care where cost of care remains high and standards of treatment remains questionable. </p>
                            <p>While both rural and urban area of India have its share of challenges in organising health care, urban health system is posing new challenges and complexities in the recent past. Patient health seeking behaviour as well as organisation
                                of urban health care system are quite complex. Rapid urbanisation and large scale migration pose challenge to organisation of effective health service delivery. Health needs and challenges are complex with presence of pluralistic
                                health systems and multiple stakeholders in urban areas; hence, there is an urgent need for a comprehensive and multipronged approach to address such complex issues.<br></p>
                            <p>With this context, KHPT is striving to design, develop, implement and evaluate a comprehensive primary health care (CPHC) model in Mysuru city through an implementation research design leveraging on learning and experience from
                                an ongoing pilot project on Non-Communicable Disease (NCD) care. The ongoing pilot aims to develop a primary health care model for NCDs and has been in operation in the same city or last three years. Given the comprehensive
                                health system management needs and disease management protocols for NCDs; many learnings from a such program could be adapted while designing a comprehensive primary care model (CPHC model). While the overarching CPHC model/theme
                                aims to transform Mysuru city as a healthy city and provide quality essential primary care services, NCDs have remained so far the main fulcrum towards that vision.&nbsp;<br></p>
                        </div>
                        <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Close</button></div>
                    </div>
                </div>
            </div>
        </div>
        <section id="Comprehensive-Primary" class="banner">
            <div class="container-fluid container-sec-one">
                <div class="row no-gutters banner-wrap">
                    <div class="col-12 col-md-5" data-aos="fade-right">
                        <div class="health-care">
                            <h1><?php the_field('banner_left_heading') ?></h1>
                            <hr>
                            <p><?php the_field('banner__left_content') ?></p>
                            <p class="button-text">
                                <a class="btn readmore-btn ml-0" role="button" href="#" data-toggle="modal" data-target="#myModal">
                                    <?php the_field('banner_left_text') ?></a>
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 health-care-img" data-aos="fade-left"><img class="img-fluid" src="<?php the_field('banner_image_right') ?>"></div>
                </div>
            </div>
        </section>
        <section id="priotries" class="spacing-header">
            <h2 data-aos="fade-up"><?php the_field('priorities_heading') ?></h2>
            <div class="container-fluid">
                <div class="row">
                    <div class="container" id="Priorities">
                        <div class="row priorities-carousel" data-aos="fade-up">
                            <?php
                            // check if the repeater field has rows of data
                            if (have_rows('priorities')) :

                                // loop through the rows of data
                                while (have_rows('priorities')) : the_row();
                            ?>
                                    <div class="col-12 col-md-3">
                                        <div class="card">
                                            <div class="card-body p-0"><img class="img-fluid" src="<?php the_sub_field('image') ?>"></div>
                                            <div class="card-text">
                                                <p><?php the_sub_field('content') ?><br></p>
                                            </div>
                                        </div>
                                    </div>
                            <?php
                                endwhile;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="our-current-projects" class="spacing-header" style="background-image: url(<?php the_field('our_current_projects_background_image') ?>);background-position: center;  background-repeat: no-repeat;     background-size: cover;     padding-top:40px; padding-bottom:20px;">
            <div class="container" id="our-project">
                <div class="row">
                    <div class="col-12">
                        <h2 data-aos="fade-up" class="white-header"><?php the_field('our_current_projects_heading') ?></h2>
                    </div>
                </div>
                <div class="row our-projects">
                    <?php
                    // check if the repeater field has rows of data
                    if (have_rows('our_current_projects')) :
                        // loop through the rows of data
                        while (have_rows('our_current_projects')) : the_row();
                    ?>
                            <div class="col-12 col-md-4 current-wrapper">
                                <div class="current-wrap">
                                    <h4><?php the_sub_field('heading') ?></h4>
                                    <p><?php the_sub_field('content') ?></p>
                                    <p><button class="btn readmore-btn" type="button"><?php the_sub_field('link_label') ?></button></p>
                                </div>
                            </div>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </section>
        <section id="latest-update" class="spacing-header">
            <h2 class="mb-4"><?php the_field('latest_updates_heading', false, false); ?></h2>
            <div class="container">
                <div class="row">
                    <?php
                    // check if the repeater field has rows of data
                    $args = array(
                        'category_name' => 'latest-updates',
                        'posts_per_page' => '-1',
                        'order' => 'DESC'
                    );
                    $incre = 1;
                    $the_query = new WP_Query($args);

                    if ($the_query->have_posts()) :
                        while ($the_query->have_posts()) :
                            $the_query->the_post();
                            if ($incre <= 1) :
                    ?>
                                <div class="col-12 col-md-6"><img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>">
                                    <div class="lastest-wrap">
                                     <p><?php echo get_the_title(); ?></p>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="list-wrap">
                                        <ul class="list-content">
                                        <?php
                                    else :
                                        ?>
                                            <li><?php echo get_the_title(); ?></li>
                                <?php
                                    endif;
                                    $incre++;
                                endwhile;
                            endif;

                            /* Restore original Post Data */

                            wp_reset_postdata();

                                ?>
                                        </ul>
                                        <div class="button-viewall readmore-btn button3" data-text="View All"><a class="btn" role="button" href="<?php echo site_url('category/latest-updates/'); ?>">View All</a></div>
                                    </div>
                                </div>
                </div>
            </div>
        </section>
        <section id="voice_from_ground" class="spacing-header pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 spacing-header">
                        <h2 class="text-white"><?php the_field('voices_from_the_ground_heading', false, false); ?></h2>
                    </div>
                </div>
                <div id="slider" class="swiper-container voice_of_ground">
                    <div class="swiper-wrapper">
                        <?php
                        // check if the repeater field has rows of data
                        if (have_rows('voices_from_the_ground')) :
                            // loop through the rows of data
                            while (have_rows('voices_from_the_ground')) : the_row();
                        ?>
                                <div class="swiper-slide">
                                    <div class="row mt-4 mb-4">
                                        <div class="col-12 col-md-6"><img class="img-fluid" src="<?php the_sub_field('image') ?>"></div>
                                        <div class="col-12 col-sm-6">
                                            <div id="voices_ground" class="community-content text-white">
                                                <p><?php the_sub_field('paragraph_1') ?></p>
                                                <h4><?php the_sub_field('heading') ?></h4>
                                                <p><?php the_sub_field('paragraph_2') ?></p>
                                                <p><?php the_sub_field('paragraph_3') ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
        <section id="previous-project" class="spacing-header">
            <div class="container previous-project" id="desktop">
                <h2 class="text-center"><?php the_field('previuos_projects_heading', false, false); ?></h2>
                <div class="row previous-project-row">
                    <?php
                    // check if the repeater field has rows of data
                    if (have_rows('previuos_projects')) :
                        // loop through the rows of data
                        while (have_rows('previuos_projects')) : the_row();
                    ?>
                            <div class="col-12 col-md-4">
                                <div class="card">
                                    <div class="card-body"><img class="img-fluid" src="<?php the_sub_field('image') ?>"></div>
                                    <div class="card-text">
                                        <h3><?php the_sub_field('heading') ?></h3>
                                        <p><?php the_sub_field('content', false, false) ?><br></p>
                                        <a class="link-a" href="#"><?php the_sub_field('link_label') ?></a>
                                    </div>
                                </div>
                            </div>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </section>
        <section id="patners" class="spacing-header">
            <div class="container mt-4">
                <div class="row">
                    <div class="col-12">
                        <h2 class="mt-5"><?php the_field('cphc_partner_heading', false, false); ?></span></h2>
                    </div>
                </div>
                <div class="row mt-3 our-partners">
                    <?php
                    // check if the repeater field has rows of data
                    if (have_rows('cphc_partners')) :
                        // loop through the rows of data
                        while (have_rows('cphc_partners')) : the_row();
                    ?>
                            <div class="col-12 col-md-2"><img src="<?php the_sub_field('image') ?>"></div>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>
                <div class="row mt-2 our-partners-mobile text-center">
                    <?php
                    // check if the repeater field has rows of data
                    if (have_rows('cphc_partners')) :
                        // loop through the rows of data
                        while (have_rows('cphc_partners')) : the_row();
                    ?>
                            <div class="col-12 col-md-2"><img src="<?php the_sub_field('image') ?>"></div>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </section>
<?php
    endwhile;
endif;
get_footer();
?>