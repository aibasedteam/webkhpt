<footer class="footer">
    <div class="container">
        <div class="row d-xl-flex">
            <div class="col-12 col-md-4">
                <h5>About Us</h5>
                <p>KHPT is a Trust registered with the Ministry of Home Affairs under the Foreign Contribution (Regulation) Act (FCRA), 1976 and has 80G registration and 12 (a) certification.</p>
                <p>© Karnataka Health Promotion Trust(KHPT), 2020.</p>
            </div>
            <div class="col-12 col-md-8">
                <div class="row">
                    <div class="col-12 col-md-5">
                        <h5>Newsletter</h5>
                        <p>Stay update with our latest News</p>
                        <form>
                            <div class="input-group">
                                <div class="input-group-prepend"></div><input class="form-control email-input" type="text" placeholder="Enter Email Id">
                                <div class="input-group-append">
                                    <button class="btn" type="button" style="border-color: #e9873b;"><i class="fas fa-arrow-right"></i>
                                </button></div>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-md-4">
                        <h5>Follow us</h5>
                        <p>Let us be social</p>
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="fab fa-twitter-square"></i></li>
                            <li class="list-inline-item"><i class="fab fa-facebook-square"></i></li>
                            <li class="list-inline-item"><i class="fab fa-instagram"></i></li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-3">

                        <!-- <p>Collaborative&nbsp;Initiatives with&nbsp;the Government</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php wp_footer(); ?>
</footer>
