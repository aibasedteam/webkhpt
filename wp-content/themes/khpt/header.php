<?php
   /**
    * The header for our theme
    *
    * This is the template that displays all of the <head> section and everything up until <div id="content">
    *
    * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
    *
    * @package WordPress
    * @subpackage Ihat
    * @since 1.0.0
    */
   require_once('class-wp-bootstrap-navwalker.php');
   ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
   <head>
      <meta charset="<?php bloginfo('charset'); ?>" />
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <link rel="profile" href="https://gmpg.org/xfn/11" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/assets/images/favicon.ico">
      <?php wp_head(); ?>
   </head>
   <body>
      <!--Desktop Navigation Bar Start-->
      <nav class="navbar navbar-light fixed-top navigation-clean" id="site-navigation">
         <div class="container-fluid">
            <a class="navbar-brand" href="https://khpt.flyingcursor.com/">
            <?php
               $custom_logo_id = get_theme_mod('custom_logo');
               $image = wp_get_attachment_image_src($custom_logo_id, 'full');
               ?>
            <img class="img-fluid" src="<?php echo $image[0]; ?>"></a>
            <a class="nav-button  pl-3 pr-3 pr-sm-5 toggle-btn navdemo" href="#">
            <span id="nav-icon3">
            <span></span>
            <span></span>
            <span></span>
            </span>
            </a>
            <div class="pl-3 pr-3 pl-sm-5 pr-sm-5 p-sm-4 sidebar text-uppercase" id="accordion">
               <?php wp_nav_menu(array(
                  'theme_location' => 'primary-menu',
                  'depth' => 3,
                  'menu_class' => 'nav flex-column',
                  'fallback_cb' => false,
                  'container' => false,
                  'walker' => new WP_Bootstrap_Navwalker()
                  )); ?>
            </div>
         </div>
      </nav>
      <!--Desktop Navigation Bar End-->