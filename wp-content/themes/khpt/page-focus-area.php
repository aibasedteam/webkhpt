<?php

/**
 * Template Name:Focus Area
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Ihat
 * @since 1.0.0
 */
get_header();
if (have_posts()) :
    while (have_posts()) : the_post();
        ?>
        <div class="modal fade" role="dialog" tabindex="-1" id="myModal">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h1 class="modal-title"><?php the_title() ?></h1>
                        <button type="button" class="close btn-modal" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <?php the_field('banner__left_content') ?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <section id="Comprehensive-Primary" class="banner">
            <div class="container-fluid container-sec-one">
                <div class="row no-gutters banner-wrap">
                    <div class="col-12 col-md-5" data-aos="fade-up">
                        <div class="health-care">
                            <h1><?php the_title() ?></h1>
                            <hr>
                            <p><?php the_field('banner__left_content', false, false) ?>
                            <p>
                            <p class="button-text">
                                <a class="btn readmore-btn ml-0" role="button" href="#" data-toggle="modal"
                                   data-target="#myModal">
                                    <?php the_field('banner_left_text') ?></a>
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 health-care-img" data-aos="fade-up"><img class="img-fluid"
                                                                                         src="<?php the_field('banner_image_right') ?>">
                    </div>
                </div>
            </div>
        </section>
        <?php
        // check if the repeater field has rows of data
        if (have_rows('priorities')) :
            ?>
            <section id="priotries" class="spacing-header">
                <h2 data-aos="fade-up"><?php the_field('priorities_heading') ?></h2>
                <div class="container-fluid">
                    <div class="row">
                        <div class="container" id="Priorities">
                            <div class="row voice_of_ground" data-aos="fade-up">
                                <?php
                                $incre = 1;
                                // loop through the rows of data
                                while (have_rows('priorities')) : the_row();
                                    ?>
                                    <div class="col-12 col-md-3">
                                        <div class="swiper-slide">
                                            <div class="card">
                                                <div class="card-body p-0"><img class="img-fluid"
                                                                                src="<?php the_sub_field('image') ?>">
                                                </div>
                                                <div class="card-text">
                                                    <?php the_sub_field('content') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                                $incre++;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php
        endif;
        // check if the repeater field has rows of data
        if (have_rows('our_current_projects')) :
            ?>
            <section id="our-current-projects" class="spacing-header"
                     style="background-image: url(<?php the_field('our_current_projects_background_image') ?>);background-position: center;  background-repeat: no-repeat;     background-size: cover;     padding-top:40px; padding-bottom:20px;">
                <div class="container" id="our-project">
                    <div class="row">
                        <div class="col-12">
                            <h2 data-aos="fade-up"
                                class="white-header"><?php the_field('our_current_projects_heading') ?></h2>
                        </div>
                    </div>
                    <div class="row our-projects" data-aos="fade-up">
                        <?php
                        $incre = 1;
                        // loop through the rows of data
                        while (have_rows('our_current_projects')) : the_row();
                            ?>
                            <div class="col-12 col-md-4 current-wrapper">
                                <div class="current-wrap">
                                    <h4><?php the_sub_field('heading') ?></h4>
                                    <p><?php the_sub_field('short_description') ?></p>
                                    <button class="btn readmore-btn" data-toggle="modal"
                                            data-target="#myModal<?php echo $incre; ?>"><?php the_sub_field('link_label') ?></button>
                                </div>
                            </div>
                            <?php
                            $incre++;
                        endwhile;
                        ?>
                    </div>
                    <?php
                    $incre = 1;
                    // loop through the rows of data
                    while (have_rows('our_current_projects')) : the_row();
                        ?>
                        <div class="modal fade" role="dialog" tabindex="-1" id="myModal<?php echo $incre; ?>">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title"><?php the_sub_field('heading') ?></h1>
                                        <button type="button" class="close btn-modal" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-left"><?php the_sub_field('content', true, true) ?></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $incre++;
                    endwhile;
                    ?>
                </div>
            </section>
        <?php
        endif;

        // check if the repeater field has rows of data
        $args = array(
            'posts_per_page' => '5',
            'post_type' => 'newsandupdates',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'NewsandUpdate',
                    'field' => 'id',
                    'terms' => get_field('select_updates') // Where term_id of Term 1 is "1".
                )
            )
        );
        $incre = 1;
        $the_query = new WP_Query($args);
        if ($the_query->have_posts()) :
            ?>
            <section id="latest-update" class="spacing-header">
                <h2 class="mb-4" data-aos="fade-up"><?php the_field('latest_updates_heading', false, false); ?></h2>
                <div class="container">
                    <div class="row">
                        <?php
                        while ($the_query->have_posts()) :
                        $the_query->the_post();
                        if ($incre <= 1) :
                        ?>
                        <div class="col-12 col-md-6" data-aos="fade-up">
                            <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>">
                            <div class="lastest-wrap">
                                <p><?php echo get_the_title(); ?></p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6" data-aos="fade-up">
                            <div class="list-wrap">
                                <ul class="list-content">
                                    <?php
                                    else :
                                        ?>
                                        <li><?php echo get_the_title(); ?></li>
                                    <?php
                                    endif;
                                    $incre++;
                                    endwhile;
                                    ?>
                                </ul>
                                <a class="custom-btn readmore-btn" href="<?php echo site_url('new-updates'); ?>"
                                   data-text="View More">
                                    <span class="btn" role="button" href="#">View More</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php
        endif;

        /* Restore original Post Data */

        wp_reset_postdata();
        // check if the repeater field has rows of data
        if (have_rows('voices_from_the_ground')) :
            ?>
            <section id="voice_from_ground" class="spacing-header pb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-12 spacing-header">
                            <h2 class="text-white"
                                data-aos="fade-up"><?php the_field('voices_from_the_ground_heading', false, false); ?></h2>
                        </div>
                    </div>
                    <div id="slider" class="swiper-container voice_of_ground">
                        <div class="swiper-wrapper" data-aos="fade-up">
                            <?php
                            // loop through the rows of data
                            while (have_rows('voices_from_the_ground')) : the_row();
                                ?>
                                <div class="swiper-slide">
                                    <div class="row mt-4 mb-4">
                                        <div class="col-12 col-md-6"><img class="img-fluid"
                                                                          src="<?php the_sub_field('image') ?>"></div>
                                        <div class="col-12 col-sm-6">
                                            <div id="voices_ground" class="community-content text-white">
                                                <?php the_sub_field('paragraph_1') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            endwhile;
                            ?>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </section>
        <?php
        endif;
        // check if the repeater field has rows of data
        if (have_rows('previuos_projects')) :
            ?>
            <section id="previous-project" class="spacing-header">
                <div class="container previous-project" id="desktop">
                    <h2 class="text-center"><?php the_field('previuos_projects_heading', false, false); ?></h2>
                    <div class="row previous-project-row" data-aos="fade-up">
                        <?php
                        $incre = 1;
                        // loop through the rows of data
                        while (have_rows('previuos_projects')) : the_row();
                            ?>
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body"><img class="img-fluid" src="<?php the_sub_field('image') ?>">
                                    </div>
                                    <div class="card-text">
                                        <h3><?php the_sub_field('heading') ?></h3>
                                        <p><?php the_sub_field('content') ?></p>
                                        <?php if (get_sub_field('link_url')) : ?>
                                            <a class="link-a" href="<?php the_sub_field('linl_url'); ?>"
                                               target="_blank"><?php the_sub_field('link_label') ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endwhile;
                        $incre++;
                        ?>
                    </div>
                </div>
            </section>
        <?php
        endif;
        // check if the repeater field has rows of data
        if (have_rows('cphc_partners')) :
            ?>
            <section id="patners" class="spacing-header">
                <div class="container mt-4">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="mt-5"
                                data-aos="fade-up"><?php the_field('cphc_partner_heading', false, false); ?></span></h2>
                        </div>
                    </div>
                    <div class="row mt-3 our-partners" data-aos="fade-up">
                        <?php
                        // loop through the rows of data
                        while (have_rows('cphc_partners')) : the_row();
                            ?>
                            <div class="col-12 col-md-2"><img src="<?php the_sub_field('image') ?>"></div>
                        <?php
                        endwhile;

                        ?>
                    </div>
                </div>
            </section>
        <?php
        endif;
    endwhile;
endif;
get_footer();
?>