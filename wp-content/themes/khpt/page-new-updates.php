<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Ihat
 * @since 1.0.0
 */
get_header();
if (have_posts()) :
   while (have_posts()) : the_post();

      $terms = get_terms(array(
         'taxonomy' => 'NewsandUpdate',
         'hide_empty' => false,
      ));

      $selectednewsupdates = '';
      if (isset($_GET['newsupdates'])) {
         $selectednewsupdates = $_GET['newsupdates'];
      }
?>
      <!-- Banner New And Updates -->
      <?php get_template_part('template/banner', 'inside'); ?>
      <!-- Banner New and Updates -->
      <section id="team-filter-sec">
         <div class="container">
            <form id="team-filter" class="form-inline" method="get">
               <div class="form-row form-right">
                  <div class="form-group">
                     <p class="select-wrapper">
                        Filter by :&nbsp;
                        <span>
                           <select class="form-control filter-control size-select" name="newsupdates">
                              <option value="" selected="">Select one</option>
                              <?php foreach ($terms as $item) {
                              ?>
                                 <option value="<?php echo $item->slug; ?>" <?php echo ($selectednewsupdates == $item->slug) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                              <?php
                              } ?>
                           </select>
                        </span>
                     </p>
                  </div>
               </div>
               <div class="form-row form-left">
                  <div class="form-group">
                     <a href="#" class="btn custom-button green-btn" id="btn-submit">Filter </a>
                  </div>
               </div>
            </form>
         </div>
      </section>
      <script>
         jQuery("#btn-submit").click(function() {
            jQuery("#team-filter").submit();
         })
      </script>
      <?php
      if (!empty($selectednewsupdates)) {
         $tax_query = array(
            array(
               'taxonomy' => 'NewsandUpdate',
               'field' => 'slug',
               'terms' => $selectednewsupdates,
               'compare' => 'LIKE',
            ),
         );
      }

      $args = array(
         'post_type' => 'newsandupdates',
         'post_status' => 'publish',
         'posts_per_page' > '6',
         'order' => 'ASC',
         'tax_query' => $tax_query,
      );
      $incre = 1;
      $the_query = new WP_Query($args);
      if ($the_query->have_posts()) :
      ?>
         <section id="news" class="spacing-header">
            <div class="container" id="updates">
               <div class="row" data-aos="fade-up">
                  <?php
                  while ($the_query->have_posts()) :
                     $the_query->the_post();
                     if ($incre <= 3) :
                  ?>
                        <div class="col-12 col-md-4 mt-3">
                           <div class="card">
                              <div class="card-body"><img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"></div>
                              <div id="news" class="card-text">
                                 <a href="">
                                    <h6><?php echo get_the_date('jS M, Y'); ?></h6>
                                    <p><?php echo get_the_title(); ?></p>
                                    <h6 class="text-orange">ReadMore&nbsp; &gt;</h6>
                                 </a>
                              </div>
                           </div>
                        </div>
                     <?php
                     else :
                     ?>
                        <div class="col-12 col-md-4">
                           <div class="card">
                              <div id="news" class="card-text">
                                 <a href="">
                                    <h6><?php echo get_the_date('jS M, Y'); ?></h6>
                                    <p><?php echo get_the_title(); ?></p>
                                    <h6 class="text-orange" href="<?php echo get_permalink(); ?>">ReadMore&nbsp; &gt;</h6>
                                 </a>
                              </div>
                           </div>
                        </div>
                  <?php
                     endif;
                     $incre++;
                  endwhile;
                  ?>
                  <div class="col-12" data-text="View All">
                     <div class="custom-btn my-4 button2 news" data-text="<?php the_field('text_view_all'); ?>">
                        <?php
                        $target = "_self";
                        if (get_sub_field('open_new_tab')) :
                           $target = "_blank";
                        endif;
                        ?>
                        <a class="btn text-white show_more" role="button" id="show_more_main<?php echo get_the_ID(); ?>" href="<?php the_sub_field('link_url'); ?>" target="<?php echo $target; ?>">Load More</a>
                     </div>
                  </div>
               </div>
            </div>
         </section>
<?php
      endif;
      wp_reset_postdata();
   endwhile;
endif;
get_footer();
?>