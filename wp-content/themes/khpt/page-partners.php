<?php
   /**
    * The main template file
    *
    * This is the most generic template file in a WordPress theme
    * and one of the two required files for a theme (the other being style.css).
    * It is used to display a page when nothing more specific matches a query.
    * E.g., it puts together the home page when no home.php file exists.
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package WordPress
    * @subpackage Ihat
    * @since 1.0.0
    */
   get_header();
   if (have_posts()) :
       while (have_posts()) : the_post();
   ?>
<!-- Our Partners -->
<?php get_template_part('template/banner', 'inside'); ?>
<!-- Our Partners -->
<?php
   // check if the repeater field has rows of data
   if (have_rows('partners_image')) :
   ?>
<section id="partnership" class="spacing-header">
   <div class="container" id="updates">
      <div class="row" id="card-images">
         <?php
            // loop through the rows of data
            while (have_rows('partners_image')) : the_row();
            ?>
         <div class="col-12 col-md-3" data-aos="fade-up">
            <div class="card">
               <div class="card-body"><img class="img-fluid" src="<?php the_sub_field('image'); ?>"></div>
            </div>
         </div>
         <?php
            endwhile;
            
            ?>
      </div>
   </div>
</section>
<?php
   endif;
   endwhile;
   endif;
   get_footer();
   ?>
COPY TO CLIPBOARD