<?php
   /**
    * Template Name: Reports
    *
    * This is the most generic template file in a WordPress theme
    * and one of the two required files for a theme (the other being style.css).
    * It is used to display a page when nothing more specific matches a query.
    * E.g., it puts together the home page when no home.php file exists.
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package WordPress
    * @subpackage Ihat
    * @since 1.0.0
    */
   get_header();
   if (have_posts()) :
       while (have_posts()) : the_post();
   ?>
<!-- Our Work with US -->
<section id="contact-us" class="banner">
   <div class="container-fluid container-sec-one">
      <div class="row no-gutters banner-wrap" 
         style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);
         background-repeat: no-repeat;
         background-size: cover;
         background-position: center;
         ">
         <div class="col-12 text-center">
            <div class="health-care">
               <h1 data-aos="fade-up"><?php echo get_the_title(); ?></h1>
               <hr style="margin-left: 45%;">
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Our Work With Us -->
<section id="current-opening" class="my-lg-3">
   <div class="container">
      <?php
         if(have_rows('reports_data')):
            while (have_rows('reports_data')):
               the_row();
                  ?>
      <div class="row my-3" data-aos="fade-up">
         <div class="col-12 col-md-8 text-center text-md-left current-opening-wrap">
            <?php 
               if(get_sub_field('image')):
               echo wp_get_attachment_image(get_sub_field('image'), 'full', false, ["class" => "img-fluid"]); 
               endif;
               ?>
            <h1><?php echo get_sub_field('heading'); ?></h1>
         </div>
         <div class="col-12 col-md-4 text-center text-md-right">
            <a class="btn apply-btn" href="<?php echo get_sub_field('pdf'); ?>" target="_blank">
            <img src="<?php echo get_template_directory_uri();?>/assets/img/reports-download.png" class="img-fluid">
            </a>
         </div>
         <div class="col-12 border-hr">
            <hr>
         </div>
      </div>
      <?php
         endwhile;
         endif;
         ?>
   </div>
</section>
<?php
   endwhile;
   endif;
   get_footer();
   ?>